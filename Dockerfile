FROM python:3.9-slim as base

# Set working directory
ENV FLYWHEEL="/flywheel/v0"
WORKDIR ${FLYWHEEL}

# Dev install. git for pip editable install and gcc
RUN apt-get update && apt-get install -y git && \
    apt-get install -y apt-transport-https
RUN apt-get -y install gcc

# Installing main dependencies
COPY requirements.txt $FLYWHEEL/
RUN pip install --no-cache-dir -r $FLYWHEEL/requirements.txt

# Installing the current project (most likely to change, above layer can be cached)
COPY ./ $FLYWHEEL/
RUN pip install --no-cache-dir .
###
# Note this is assume that you have created a `.dockerignore` file on your repo.
# See below to learn more about the `.dockerignore` file.
###

# Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py
ENTRYPOINT ["python","/flywheel/v0/run.py"]