"""Since this module is imported in .imgreaders, it should not import anything from .imgreaders."""

import logging
from abc import ABC, abstractmethod

import numpy as np
import SimpleITK as sitk

log = logging.getLogger(__name__)


class ImagePreparer(ABC):
    """Depending on what ImageReader was instantiated, this should instantiate its associated ImagePreparer."""

    @classmethod
    def factory(cls, image_prep_name: str, config_dict: dict):
        """Returns an instance of the subclass."""
        subclasses = cls.__subclasses__()
        subclass_names = [sub.__name__ for sub in subclasses]
        for subclass in subclasses:
            if subclass.__name__ == image_prep_name:
                return subclass(config_dict=config_dict)
        raise ValueError(
            "Could not find subclass of class %s named %s, could not "
            "create instance.\n"
            "Available names: \n\t%s" % (cls.__name__, image_prep_name, subclass_names)
        )

    def __init__(self, config_dict: dict):
        """Initialize the instance's attributes."""
        self.config_dict = config_dict

    @abstractmethod
    def prepare_image(self, image_sitk: sitk.Image):
        """Prepares the image for calculating radiomic features."""
        pass

    @staticmethod
    def _prepare_image(image_sitk: sitk.Image):
        """Performs static methods applicable to all ImagePreparer isntances.

        The following methods are applied:
        - ImagePreparer.check_sitk_dim()
        - ImagePreparer.check_sitk_num_channels()

        Params
        ------
        image_sitk: sitk.Image

        Returns
        -------
        image_sitk: sitk.Image
        """
        image_sitk = ImagePreparer.check_sitk_dim(image_sitk=image_sitk)
        return ImagePreparer.check_sitk_num_channels(image_sitk=image_sitk)

    @staticmethod
    def check_sitk_num_channels(image_sitk: sitk.Image):
        """Returns a single-channel sitk image.

        Checks if the image has 3 channels, and returns a single-channel image
        if so. Returns the passed image if it's already single-channel, and
        throws an error if it has any other number of channels.
        """
        if image_sitk.GetNumberOfComponentsPerPixel() == 3:
            if image_sitk.GetDimension() != 2:
                raise AttributeError("Unable to process 3D color volumes at this time.")
            log.warning(
                "No color/rgb images allowed, using only the first "
                "channel to calculate radiomics"
            )
            return sitk.VectorIndexSelectionCast(image_sitk, 0)
        elif image_sitk.GetNumberOfComponentsPerPixel() == 1:
            return image_sitk
        else:
            raise AttributeError(
                "Only single or 3-channel images are allowed, got %s"
                % image_sitk.GetNumberOfComponentsPerPixel()
            )

    @staticmethod
    def check_sitk_dim(image_sitk: sitk.Image):
        """Checks if the image is 2D or 3D, throws error otherwise."""
        if image_sitk.GetDimension() in (2, 3):
            return image_sitk
        else:
            raise AttributeError(
                "Only 2 or 3d images can be used, got %s" % image_sitk.GetDimension()
            )


class NumpyImagePreparer(ImagePreparer):
    """Class to handle numpy images."""

    @staticmethod
    def use_only_first_image(image_sitk: sitk.Image):
        """Returns only the first image in the simlpleITK image if it's a 3D image and if it has 3 images along the third dimension, where each image represents a single channel."""
        log.info("Attempting to use only the first image in the numpy array...")

        if image_sitk.GetDimension() == 3:
            if image_sitk.GetSize()[-1] != 3:
                log.info(
                    "\tThe third dimension did not have 3 images (each image "
                    "representing one channel). Returning passed image."
                )
                return image_sitk
            log.warning(
                "\t3D array found. Using only the first image located "
                "in [:, :, 0] to calculate radiomics."
            )
            return image_sitk[:, :, 0]
        elif image_sitk.GetDimension() == 2:
            log.info("\tGot 2D image already. Returning passed image.")
            return image_sitk
        else:
            raise AttributeError(
                "Only 2 or 3d images can be used, got %s" % image_sitk.GetDimension()
            )

    def prepare_image(self, image_sitk: sitk.Image):
        """In addition to methods defined in self._prepare_image, convert the numpy image to single channel if the the config option `convert_numpy_to_single_channel` is set to True."""
        image_sitk = self._prepare_image(image_sitk=image_sitk)

        if self.config_dict.get("convert_numpy_to_single_channel") is True:
            log.info("Config option convert_numpy_to_single_channel set to True. ")
            return self.use_only_first_image(image_sitk=image_sitk)
        else:
            return image_sitk


class SimpleITKImagePreparer(ImagePreparer):
    """Class to handle SimpleITK images."""

    def prepare_image(self, image_sitk: sitk.Image):
        """Prepare image with methods defined in self._prepare_image."""
        return self._prepare_image(image_sitk=image_sitk)


class DicomZipDImagePreparer(SimpleITKImagePreparer, ImagePreparer):
    """Inherit methods from SimpleITKImagePreparer."""


class EmptyMaskImagePreparer(ImagePreparer):
    """Class to handle FileNotFoundError. This is to handle when no mask is passed."""

    @staticmethod
    def create_mask_from_image(image_sitk: sitk.Image):
        """Returns a mask created from its corresponding input image.

        In addition to the size, geometry parameters also get set according
        to the input image. See the following for more on metadata geometry
        parameters:

            https://simpleitk.org/SPIE2018_COURSE/images_and_resampling.pdf

        Params
        ------
        image_sitk: sitk.Image
            The corresponding input image from which the mask will be built.

        Returns
        -------
        image_sitk_out: sitk.Image
            The mask created from its corresponding input image.
        """
        log.warning(
            "Mask path does not exist. Creating mask of almost "
            "entire image, excluding one single pixel at the top-left"
            "corner (0, 0).\n"
            "Shape features will not be calculated."
        )
        if image_sitk.GetDimension() == 3:
            # Create 2D mask
            num_cols, num_rows, num_images = image_sitk.GetSize()
            mask_array = np.ones((num_images, num_rows, num_cols))
            mask_array[:, 0, 0] = 0
            image_sitk_out = sitk.GetImageFromArray(mask_array)
        elif image_sitk.GetDimension() == 2:
            # Create 2D mask
            num_cols, num_rows = image_sitk.GetSize()
            if num_rows <= 0 and num_cols <= 0:
                log.error("Image size is 0. Unable to create mask.")
                return None
        else:
            raise AttributeError(
                "Only 2 or 3d images can be used, got %s" % image_sitk.GetDimension()
            )

        # Copy geometry parameters from source image
        image_sitk_out.CopyInformation(image_sitk)
        return image_sitk_out

    def prepare_image(self, image_sitk: sitk.Image):
        """Returns a mask created from the input image.

        Warning: if config option `convert_numpy_to_single_channel` in
        config_dict is set to True, this will cause problems if the input image
        was not numpy type and also has exactly 3 images along the 3rd
        dimension.
        """
        if not isinstance(image_sitk, sitk.Image):
            log.error("Input image is not of type SimpleITK.Image.")
            return None

        image_sitk = self.create_mask_from_image(image_sitk=image_sitk)

        if isinstance(image_sitk, type(None)):
            log.error("Unable to create mask from image.")
            return None

        if self.config_dict["convert_numpy_to_single_channel"] is True:
            log.warning(
                "Option convert_numpy_to_single_channel set to True and "
                "mask was created from input image. This can unintentionally "
                "cause the mask to use only the first image in the 3D array if"
                " the input image was not numpy type and has exactly 3 "
                "images along the 3rd dimension."
            )
            return NumpyImagePreparer.use_only_first_image(image_sitk=image_sitk)
        else:
            return image_sitk
