"""Module to handle different image readers."""
import logging
import os
from abc import ABC, abstractmethod
from tempfile import TemporaryDirectory

import numpy as np
import SimpleITK as sitk
from fw_file.dicom import DICOMSeries

from fw_gear_pyradiomics.imgprep import (
    DicomZipDImagePreparer,
    EmptyMaskImagePreparer,
    NumpyImagePreparer,
    SimpleITKImagePreparer,
)

log = logging.getLogger(__name__)


class ImageReader(ABC):
    """Abstract class to read images from different file formats."""

    @property
    @abstractmethod
    def image_prep_name(self):
        """The name of its associated image preparer."""
        pass

    def __init__(self, path_image: str):
        """Initialize the instance's attributes."""
        self.path_image = path_image

    @classmethod
    @abstractmethod
    def _from_factory(cls, path_image: (str, os.PathLike)):
        """Check if requirements are met before instatiation; return None if requirements are not met."""
        pass

    @classmethod
    def factory(cls, path_image: (str, os.PathLike)):
        """Instantiate and return the image reader object."""
        path_dir, file_name = os.path.split(path_image)

        result = None

        # if path is empty or non exisiting, return and empty image reader. This is to handle
        # when no mask is passed
        if not os.path.exists(path_image):
            return EmptyMaskImageReader._from_factory(path_image=path_image)

        # Try to handle the file with all except SimpleITKImageReader. Use this
        # as last resort
        subclasses_min_some = [
            subclass
            for subclass in cls.__subclasses__()
            if subclass.__name__
            not in [SimpleITKImageReader.__name__, EmptyMaskImageReader.__name__]
        ]

        for subclass in subclasses_min_some:
            log.debug("Attempting to create object with %s" % subclass.__name__)
            result = subclass._from_factory(path_image=path_image)
            if result:
                log.info("Created object wit class %s" % subclass.__name__)
                break

        if not result:
            log.debug(
                "Could not open file with other classes. Attempting to "
                "open with %s" % SimpleITKImageReader.__name__
            )
            result = SimpleITKImageReader._from_factory(path_image=path_image)
            if result:
                log.info(
                    "File '%s' can be handled by %s, created %s "
                    "object."
                    % (
                        path_image,
                        SimpleITKImageReader.__name__,
                        SimpleITKImageReader.__name__,
                    )
                )

        # Throw error if we could not handle the input image
        if not result:
            raise ValueError(
                f"Could not find instantiation of class {cls.__name__} able to "
                f"handle file {file_name}."
            )

        return result

    @abstractmethod
    def read_image(self, path_image: (str, os.PathLike) = None):
        """Returns a SimpleITKImage.

        Abstract method that reads images stored in different file formats.
        """


class DicomZipImageReader(ImageReader):
    """Class to handle zipped dicom files."""

    image_prep_name = DicomZipDImagePreparer.__name__

    @classmethod
    def _from_factory(cls, path_image: (str, os.PathLike)):
        # Return None if an error happens or the zipped dicom can't be read by
        # DICOMSeries.from_zip
        try:
            series = DICOMSeries.from_zip(path_image)
            if series is None:
                return None
            return cls(path_image=path_image)

        except Exception as e:
            log.info(e)
            return None

    def read_image(self, path_image: (str, os.PathLike) = None):
        """Reads the image with SimpleITK."""
        if path_image is None:
            path_image = self.path_image

        # Unzip the dicom series in a temp directory and read all the files
        # with a SimpleITK reader
        series = DICOMSeries.from_zip(path_image)
        with TemporaryDirectory() as tmpdirname:
            series.to_dir(dirpath=tmpdirname)
            reader = sitk.ImageSeriesReader()

            dicom_names = reader.GetGDCMSeriesFileNames(tmpdirname)
            reader.SetFileNames(dicom_names)

            return reader.Execute()


class NumpyImageReader(ImageReader):
    """Class to handle numpy arrays. This is to handle when no mask is passed."""

    image_prep_name = NumpyImagePreparer.__name__

    @classmethod
    def _from_factory(cls, path_image: (str, os.PathLike)):
        # Check if the file can be opened with numpy; only numpy arrays should
        # be opened (i.e., do not set allow_pickle=True)
        try:
            image_array = np.load(path_image)
        except (ValueError, TypeError, FileNotFoundError) as e:
            log.info(e)
            return None
        # Log the loaded image array information
        log.info(f"Loaded image array shape: {image_array.shape}")

        return cls(path_image=path_image)

    pass

    def read_image(self, path_image: (str, os.PathLike) = None):
        """Reads the image with numpy and converts it to SimpleITK."""
        if path_image is None:
            path_image = self.path_image
        image_arr = np.load(path_image)

        # Move axes to corresponding shape in simple_itk
        image_arr = np.moveaxis(image_arr, -1, 0)
        return sitk.GetImageFromArray(image_arr)


class SimpleITKImageReader(ImageReader):
    """Class to handle SimpleITK images. This is the default reader."""

    image_prep_name = SimpleITKImagePreparer.__name__

    @classmethod
    def _from_factory(cls, path_image: (str, os.PathLike)):
        # Check if the file can be opened with SimpleITK
        try:
            sitk.ReadImage(path_image)
        except (RuntimeError, TypeError) as e:
            log.info(e)
            return None

        return cls(path_image=path_image)

    def read_image(self, path_image: (str, os.PathLike) = None):
        """Reads the image with SimpleITK."""
        if not path_image:
            path_image = self.path_image
        return sitk.ReadImage(path_image)

    pass


class EmptyMaskImageReader(ImageReader):
    """Class to handle FileNotFoundError. This is to handle when no mask is passed."""

    image_prep_name = EmptyMaskImagePreparer.__name__

    @classmethod
    def _from_factory(cls, path_image: (str, os.PathLike)):
        return cls(path_image=path_image)

    def read_image(self, path_image: (str, os.PathLike) = None):
        """Returns an empty SimpleITK object.

        Since this is empty, don't need to do anything here. Prepare the
        empty image with its ImagePreparer object.
        Params
        ------
        path_image: str, os.PathLike
        """
        return sitk.Image()
