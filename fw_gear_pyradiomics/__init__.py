"""The fw_gear_pyradiomics package."""
import traceback  # Import the traceback module
from importlib import metadata

pkg_name = __package__ if __package__ else "fw_gear_pyradiomics"

try:
    __version__ = metadata.version(__package__)
except Exception as ex:
    traceback.print_exc()
    print(ex)
    __version__ = "2.1.0_3.1.0"
