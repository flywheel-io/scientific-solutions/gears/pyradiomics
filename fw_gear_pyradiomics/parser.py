"""Parser module to parse gear config.json."""

import logging
from typing import Tuple

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_pyradiomics.pyrad import PyradFactory, PyradFactoryParams

log = logging.getLogger(__name__)


def parse_confi_dict(config_dict):
    """Returns a parsed and formatted config dict."""


def parse_config(
    gear_context: GearToolkitContext,
) -> Tuple[bool, PyradFactory]:
    """Parses gear_context's config.json file and returns relevant inputs and options.

    Returns
    -------
    debug: bool
        Whether to log in debug mode
    pyrad_factory: Pyrad
        A concrete instantiation of the abstract class PyradFactory.
    """
    log.info("Parsing inputs from gear context...")

    # Get inputs and config
    path_image = gear_context.get_input_path("image")
    path_mask = gear_context.get_input_path("mask")
    debug = gear_context.config.get("debug")
    config_dict = gear_context.config
    path_params = gear_context.get_input_path("params_file")

    params = PyradFactoryParams(
        path_image=path_image,
        path_mask=path_mask,
        output_dir=gear_context.output_dir,
        config_dict=config_dict,
        path_params=path_params,
    )

    # Use an interface for handling different file types
    pyrad_factory = PyradFactory(params)
    log.info("\tFinished parsing.")

    return debug, pyrad_factory
