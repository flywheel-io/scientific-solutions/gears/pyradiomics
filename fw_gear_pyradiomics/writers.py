"""Objects for writing out files and metadata to the flywheel platform."""
import logging
from abc import ABC, abstractmethod
from collections import OrderedDict

import SimpleITK as sitk
from flywheel_gear_toolkit import GearToolkitContext

log = logging.getLogger(__name__)


class FlywheelWriter(ABC):
    """Object to write out objects/files to the flywheel platform."""

    def __init__(self, gear_context: GearToolkitContext):
        """Initialize the object."""
        self.gear_context = gear_context


class FlywheelHandler:
    """Object for getting and setting objects/info using a gear's context."""

    def __init__(self, gear_context: GearToolkitContext):
        """Initialize the object."""
        self.gear_context = gear_context

    @staticmethod
    def convert_numpy_arr_to_list(input_dict: dict):
        """Converts numpy arrays to lists in a dictionary."""
        pass


class FileInfoFlywheelHandler(FlywheelHandler):
    """Object for getting file info from a gear's context."""

    def get_input_file_info(self, input_name: str):
        """Returns the file info for the given input name, raises error if no input name is found.

        Params
        ------
        input_name: str
            The name of the input parameter, not the argument name. For example,
            if the file with filename `my_mri_scan.dicom.zip` is the argument
            for the input param name `input_image`, `input_name` should be
            `input_image`.
        """
        input_file = self.gear_context.get_input(name=input_name)
        if input_file is None:
            raise ValueError(
                "input_name '%s' was not found in context. Only the following"
                "are available:\n\t %s"
                % (input_name, list(self.gear_context.manifest["inputs"].keys()))
            )
        return input_file["object"]["info"]

    def get_gear_dict_from_file(self, input_name: str):
        """Returns the file's gear dict, i.e., file.info.gears dictionary.

        Returns None if nothing is found.

        Params
        ------
        input_name: str
            The name of the input parameter, not the argument name. For example,
            if the file with filename `my_mri_scan.dicom.zip` is the argument
            for the input param name `input_image`, `input_name` should be
            `input_image`.
        """
        file_info = self.get_input_file_info(input_name=input_name)
        return file_info.get("gears")


class GearInfoFlywheelHandler(FlywheelHandler):
    """Object for getting and setting gear info using a gear's context."""

    def get_gear_version(self):
        """Returns the gear's version."""
        return self.gear_context.manifest["version"]

    def get_gear_name(self):
        """Returns the gear's name."""
        return self.gear_context.manifest["name"]

    def create_gear_info_dict(self):
        """Returns a dictionary with the gear's name, version, and config settings."""
        return {
            "name": self.get_gear_name(),
            "version": self.get_gear_version(),
            "config": self.gear_context.config,
        }

    @staticmethod
    def check_gear_info_dict(gear_info_dict: dict):
        """Raises and error if gear_info_dict does not have the correct structure.

        gear_info_dict: dict
            the nested dictionary info['gears']['gear_name']['gear_info']
        """
        gear_info_keys = ["name", "version", "config"]
        if gear_info_keys != list(gear_info_dict.keys()):
            raise ValueError(
                "gear_info dict does not have correct keys."
                "See %s.create_base_gear_dict() for correct structure."
                "Gear dict: \n\t%s"
            )

    def check_gear_dict(self, gear_dict: dict):
        """Raises error if the gear dict does not follow the correct format/structure."""
        # Check first level
        if "gears" not in gear_dict:
            raise ValueError(
                "The gear dict must have 'gears' as the top-level key. "
                "Gear dict: \n\t%s" % gear_dict
            )
        if len(gear_dict.keys()) != 1:
            raise ValueError(
                "The gear dict can only have 'gears' as the top-level key."
                "Gear dict: \n\t%s" % gear_dict
            )

        # Check that each each gear has correct gear_info dict
        for gear_name, gear_vals in gear_dict["gears"].items():
            # Check if gear_info exists for current gear
            if "gear_info" not in gear_vals:
                raise ValueError(
                    "gear_info dict not found in gear name key '%s'. "
                    "See %s.create_base_gear_dict() for correct structure."
                    "Gear dict: \n\t%s" % (gear_name, self.__name__, gear_dict)
                )

            # Check gear_info dict
            self.check_gear_info_dict(gear_vals["gear_info"])

    def update_gear_info_dict(self, gear_dict: dict):
        """Returns a gear_dict with the updated 'gear_info' dict.

        See self.create_base_gear_dict() for structure.
        """
        self.check_gear_dict(gear_dict=gear_dict)
        gear_info = self.create_gear_info_dict()
        gear_dict["gears"][self.get_gear_name()]["gear_info"].update(gear_info)
        return gear_dict

    def add_metadata_to_gear_dict(self, gear_dict: dict, gear_metadata: dict):
        """Returns a gear dict with added gear metadata.

        It checks the gear dict structure first. If any keys in gear_metadata
        are 'gear_info', a warning will be thrown that this replace the current
        gear_info values. Ensure that this is intentional. The gear_info dict
        gets automatically updated whenever gear metadata are added.

        Params
        ------
        gear_dict: dict
            A dict with the structure defined in
            GearInfoFlywheelHandler.create_base_gear_dict().
        gear_metadata: dict
            Metadata to add to gear_dict['gears']['gear_name']. Throws warning
            if this dict contains the key 'gear_info'
        """
        self.check_gear_dict(gear_dict=gear_dict)

        # check metadata dict
        if "gear_info" in gear_metadata:
            log.warning(
                "'gear_info' key found in gear_metadata and will replace the "
                "current gear_info values. Ensure that this is intentional. "
                "The gear_info dict gets automatically updated whenever "
                "gear metadata are added."
            )
            self.check_gear_info_dict(gear_info_dict=gear_metadata["gear_info"])
        else:
            # update the gear_info dict
            gear_dict = self.update_gear_info_dict(gear_dict=gear_dict)

        # Add metadata to the gear dict
        gear_matadata_current = gear_dict["gears"][self.get_gear_name()]
        gear_matadata_current.update(gear_metadata)
        gear_dict["gears"][self.get_gear_name()].update(gear_matadata_current)
        return gear_dict

    def replace_metadata_in_gear_dict(self, gear_dict: dict, gear_metadata: dict):
        """FUTURE: Replace all metadata found in info['gears']['gear_name'].

        This also updates the 'gear_info' dict. See self.create_base_gear_dict()
        for gear_dict structure. This method is not yet implemented.
        """

    def create_base_gear_dict(self):
        """Returns a dict with the correct structure for storing metadata from a gear run to the info dictionary.

        This dict should be stored in the info dictionary. The top-level key is
        'gears', and the structure is as follows:

            {'gears':
                {'gear_name':
                    {'gear_info': {
                        'name': 'gear_name',
                        'version': 'gear_version',
                        'config': gear_config_settings}
                    gear_run_key_1: val_1,
                        ...
                    gear_run_key_n: val_n
                }
            }
        """
        gear_info = self.create_gear_info_dict()
        base_gear_dict = {"gears": {self.get_gear_name(): {"gear_info": gear_info}}}
        return base_gear_dict


class CsvFlywheelWriter(FlywheelWriter):
    """Abstract object for writing out csv files to Flywheel."""

    def __init__(self, gear_context: GearToolkitContext):
        """Initialize the object."""
        super().__init__(gear_context=gear_context)

    @abstractmethod
    def write_features_to_csv(self, features: (OrderedDict, sitk.Image)):
        """Abstract method for writing out a csv file.

        If a voxel analysis is
        requested, the features are returned as an SimpleITK image instead of
        an ordered dict, so it has to be determined how this should be
        implemented for that case.

        """
        raise NotImplementedError


class FileFlywheelWriter(
    FlywheelWriter, FileInfoFlywheelHandler, GearInfoFlywheelHandler
):
    """Abstract object for writing to Flywheel FileEntry objects."""

    def __init__(self, gear_context: GearToolkitContext, file_object_out):
        """Initialize the object."""
        self.file_object_out = file_object_out
        super().__init__(gear_context=gear_context)

    @abstractmethod
    def write_object_to_file_info(self, input_name: str):
        """Writes the file_object_out to the file.info.gears.gear-name.

        Params
        ------
        input_name: str
            The name of the input parameter, not the argument name. For example,
            if the file with filename `my_mri_scan.dicom.zip` is the argument
            for the input param name `input_image`, `input_name` should be
            `input_image`.
        """
        raise NotImplementedError

    def update_file_gears_dict(
        self, input_name: str, gear_dict: dict, file_info: dict = None
    ):
        """Update a file's info.gears dict.

        Checks if the gears dict already exists. Since each key in the gears
        dict is a gear name, new values will replace existing values if
        any key in gear_dict matches an existing one.

        Params
        ------
        input_name: str
            The name of the input parameter, not the argument name. For example,
            if the file with filename `my_mri_scan.dicom.zip` is the argument
            for the input param name `input_image`, `input_name` should be
            `input_image`.
        gear_dict: dict
            A dict with the structure defined in
            GearInfoFlywheelHandler.create_base_gear_dict().

        """
        # Get file_info if not passed
        if file_info is None:
            file_info = self.get_input_file_info(input_name=input_name)

        # Check gear dict
        self.check_gear_dict(gear_dict=gear_dict)

        # Update if "gears" already exists in file_info dict, add dict to
        # file_info otherwise
        if "gears" in file_info:
            file_info["gears"].update(gear_dict["gears"])
        else:
            file_info.update(gear_dict)

        # update the file info in the .json file
        file_name = self.gear_context.get_input_filename(name=input_name)
        self.gear_context.update_file_metadata(file_name=file_name, info=file_info)
