"""Objects for writing out files and metadata to the flywheel platform."""
import csv
import logging
import os
from abc import abstractmethod
from collections import OrderedDict

import SimpleITK as sitk
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_pyradiomics.writers import CsvFlywheelWriter, FileFlywheelWriter

log = logging.getLogger(__name__)


def get_pyrad_flywheel_writer(features):
    """Get the concrete class that can handle the feature type."""
    pyrad_writers = {
        RadPyradFlywheelWriter: RadPyradFlywheelWriter.feature_type,
        VoxelPyradFlywheelWriter: VoxelPyradFlywheelWriter.feature_type,
    }

    for concrete_class, feature_type in pyrad_writers.items():
        if isinstance(features, feature_type):
            return concrete_class

    # raise error if reach here
    raise NotImplementedError(
        "Could not find concrete instance of class %s that can handle "
        "type %s. Could not create instance.\n"
        "Available types that can be handled: \n\t%s"
        % (PyradFlywheelWriter.__name__, type(features), list(pyrad_writers.values()))
    )


class PyradFlywheelWriter(FileFlywheelWriter, CsvFlywheelWriter):
    """A high-level abstract object for writing objects from the pyradiomics gear to Flywheel container and FileEntry objects.

    As of now, this can only handle FileEntry objects. Build and inherit more
    FlywheelWriter subclasses to handle containers.
    """

    @property
    @abstractmethod
    def feature_type(self):
        """The feature type that a concrete instance can handle."""
        pass

    @classmethod
    def factory(
        cls, gear_context: GearToolkitContext, features: (OrderedDict, sitk.Image)
    ):
        """A factory method for selecting which instantiation of PyradFlywheelWriter to return."""
        # Find the correct concrete class and return and instantiate it
        return get_pyrad_flywheel_writer(features=features)(
            gear_context=gear_context, file_object_out=features
        )

    @abstractmethod
    def write_object_to_file_info(self, input_name: str):
        """Writes the file_object_out to the file.info.gears.gear-name.

        Params
        ------
        input_name: str
            The name of the input parameter, not the argument name. For example,
            if the file with filename `my_mri_scan.dicom.zip` is the argument
            for the input param name `input_image`, `input_name` should be
            `input_image`.
        """
        pass

    @abstractmethod
    def write_features_to_csv(self, features: (OrderedDict, sitk.Image)):
        """Writes the features to a csv file."""
        raise NotImplementedError


class VoxelPyradFlywheelWriter(PyradFlywheelWriter):
    """A concrete class for writing voxel-based radiomic features to Flywheel container."""

    feature_type = sitk.Image

    def write_object_to_file_info(self, input_name: str):
        """Writes the file_object_out to the file.info.gears.gear-name."""
        pass


class RadPyradFlywheelWriter(PyradFlywheelWriter):
    """A concrete class for writing radiomic features to Flywheel container."""

    feature_type = OrderedDict

    def write_object_to_file_info(self, input_name: str):
        """Writes and OrderedDict stored in self.file_object_out to file.info.gears.gear-name.

        Params
        ------
        input_name: str
            The name of the input parameter, not the argument name. For example,
            if the file with filename `my_mri_scan.dicom.zip` is the argument
            for the input param name `input_image`, `input_name` should be
            `input_image`.
        """
        # Create a base gear dict
        gear_dict = self.create_base_gear_dict()

        # Convert OrderedDict to regular dict
        file_object_out = dict(self.file_object_out)

        # add metadata to gear dict
        gear_dict = self.add_metadata_to_gear_dict(
            gear_dict=gear_dict, gear_metadata=file_object_out
        )

        # Add OrderedDict to info.gears
        self.update_file_gears_dict(input_name=input_name, gear_dict=gear_dict)

    def write_features_to_csv(self, input_name: str, features: OrderedDict = None):
        """Writes an OrderedDict of features to a csv file. The features can be derived from a single image (2D) or multiple images (3D).

        If no features are passed, it will use self.file_object_out as the
        features.

        Returns
        -------
        path_out_csv: str
            The output path of the written csv.

        Params
        ------
        input_name: str
            The name of the input parameter, not the argument name. For example,
            if the file with filename `my_mri_scan.dicom.zip` is the argument
            for the input param name `input_image`, `input_name` should be
            `input_image`.
        """
        if not features:
            features = self.file_object_out

        path_out_csv = os.path.join(
            self.gear_context.output_dir,
            os.path.splitext(self.gear_context.get_input_filename(input_name))[0]
            + "_pyradiomics.csv",
        )
        log.info("Writing out .csv file of pyradiomic features to '%s'" % path_out_csv)
        with open(path_out_csv, "w") as out_csv:
            csvwriter = csv.writer(out_csv)
            csvwriter.writerow(dict(features))

            if isinstance(list(features.values())[0], str):
                # write out for single image
                csvwriter.writerow(dict(features).values())
                pass
            elif isinstance(list(features.values())[0], list):
                # write out for multiple images
                for val in list(
                    zip(*dict(features).values())
                ):  # have to transpose to write out each image slice as row
                    csvwriter.writerow(val)
            else:
                raise TypeError(
                    "Only type 'str' or 'list' allowed for each value of features"
                    "dict, got '%s'" % type(list(features.values())[0])
                )

        return path_out_csv
