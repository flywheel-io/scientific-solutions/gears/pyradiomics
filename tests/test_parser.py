"""Module to test parser.py"""

import os
from pathlib import Path
from unittest.mock import MagicMock

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_pyradiomics.parser import parse_config
from fw_gear_pyradiomics.pyrad import PyradFactory
from tests.test_imgprep import config_dicts


def test_parse_config(tmpdir):
    """
    Mock the gear context and some of its methods to test that we are parsing
    and getting the correct outputs
    """

    gear_context = MagicMock(spec=GearToolkitContext)
    image = os.path.abspath("tests/assets/chest_xray.png")
    mask = ""
    config_dict = config_dicts[0]

    def side_effect(input_val):
        if input_val == "image":
            return image
        elif input_val == "mask":
            return mask
        elif input_val == "params_file":
            return mask
        else:
            raise ValueError(
                "input val can only be 'image', 'mask', or 'params_file' "
                "not '%s'" % input_val
            )

    # prepare our mocker object with mock functions
    gear_context.get_input_path = side_effect
    output_dir = Path(tmpdir.mkdir("output"))
    gear_context.output_dir = output_dir
    gear_context.config = config_dict
    debug, pyrad_factory = parse_config(gear_context)

    assert pyrad_factory.path_image == image
    assert pyrad_factory.path_mask == mask
    assert pyrad_factory.config_dict == config_dict
    assert isinstance(pyrad_factory, PyradFactory)
