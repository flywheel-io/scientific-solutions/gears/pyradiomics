import pytest
import SimpleITK as sitk

from fw_gear_pyradiomics.imgprep import (
    DicomZipDImagePreparer,
    EmptyMaskImagePreparer,
    ImagePreparer,
    NumpyImagePreparer,
    SimpleITKImagePreparer,
)
from fw_gear_pyradiomics.pyrad import ImageReader

config_dicts = [
    # default params
    {"voxel_based": False, "convert_numpy_to_single_channel": False},
    # For testing numpy single channel conversion
    {"voxel_based": False, "convert_numpy_to_single_channel": True},
]


@pytest.mark.parametrize(
    "path_image, path_mask, image_preparer_classes, image_sizes, config_dict",
    [
        (  # test SimpleITK
            "tests/assets/chest_xray.png",
            "tests/assets/chest_xray_mask.nrrd",
            (SimpleITKImagePreparer, SimpleITKImagePreparer),
            ((1015, 1100), (1015, 1100)),
            config_dicts[0],
        ),
        (  # test numpy and simple itk readers
            "tests/assets/chest_xray.npy",
            "tests/assets/chest_xray_mask.nrrd",
            (NumpyImagePreparer, SimpleITKImagePreparer),
            ((1015, 1100, 3), (1015, 1100)),
            config_dicts[0],
        ),
        (  # Test numpy and empty mask
            "tests/assets/chest_xray.npy",
            "",
            (NumpyImagePreparer, EmptyMaskImagePreparer),
            ((1015, 1100, 3), (1015, 1100, 3)),
            config_dicts[0],
        ),
        (  # test numpy, simple itk readers, and
            # convert_numpy_to_single_channel
            "tests/assets/chest_xray.npy",
            "tests/assets/chest_xray_mask.nrrd",
            (NumpyImagePreparer, SimpleITKImagePreparer),
            ((1015, 1100), (1015, 1100)),
            config_dicts[1],
        ),
        (  # Test numpy, empty mask, and convert_numpy_to_single_channel
            "tests/assets/chest_xray.npy",
            "",
            (NumpyImagePreparer, EmptyMaskImagePreparer),
            ((1015, 1100), (1015, 1100)),
            config_dicts[1],
        ),
        (  # test zipped dicom and its mask
            "tests/assets/9189822998 - 5865 - Surview Test.dicom.zip",
            "tests/assets/9189822998 - 5865 - Surview Test_mask.nrrd",
            (DicomZipDImagePreparer, SimpleITKImagePreparer),
            ((512, 665, 5), (512, 665, 5)),
            config_dicts[0],
        ),
        (  # test dicom and an empty mask
            "tests/assets/9189822998 - 5865 - Surview Test.dicom.zip",
            "",
            (DicomZipDImagePreparer, EmptyMaskImagePreparer),
            ((512, 665, 5), (512, 665, 5)),
            config_dicts[0],
        ),
    ],
)
def test_image_preparers(
    path_image, path_mask, image_preparer_classes, image_sizes, config_dict
):
    # Create readers
    image_reader = ImageReader.factory(path_image=path_image)
    image_reader_mask = ImageReader.factory(path_image=path_mask)

    # Read images
    image_sitk = image_reader.read_image()
    image_sitk_mask = image_reader_mask.read_image()

    # Create preparers images
    image_preparer = ImagePreparer.factory(
        image_prep_name=image_reader.image_prep_name, config_dict=config_dict
    )
    image_preparer_mask = ImagePreparer.factory(
        image_prep_name=image_reader_mask.image_prep_name, config_dict=config_dict
    )

    # Prepare both images. pass the image_sitk to create a mask if the
    # preparer is EmptyMaskImagePreparer
    image_sitk_prepared = image_preparer.prepare_image(image_sitk=image_sitk)
    if isinstance(image_preparer_mask, EmptyMaskImagePreparer):
        image_sitk_prepared_mask = image_preparer_mask.prepare_image(
            image_sitk=image_sitk
        )
    else:
        image_sitk_prepared_mask = image_preparer_mask.prepare_image(
            image_sitk=image_sitk_mask
        )

    assert isinstance(image_preparer, image_preparer_classes[0])
    assert isinstance(image_preparer_mask, image_preparer_classes[1])
    assert isinstance(image_sitk_prepared, sitk.Image)
    assert isinstance(image_sitk_prepared_mask, sitk.Image)
    assert image_sitk_prepared.GetSize() == image_sizes[0]
    assert image_sitk_prepared_mask.GetSize() == image_sizes[1]
