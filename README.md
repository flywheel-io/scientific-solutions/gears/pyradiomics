# PyRadiomics

## Overview

### Summary

![pyradiomics](docs/pyradiomics_schema.png)

This gear runs [pyradiomics](https://pyradiomics.readthedocs.io/en/latest/index.htm),
a Python library for extracting radiomic features from medical images.
This library is used to analyze features such as shape, intensity, and texture from images,
which can be applied in machine learning for medical diagnostics.

The radiomc features will be stored in a csv file.

### Cite

*License:* MIT

### Classification

*Category:* analysis

*Gear Level:*

- [ ] Project
- [ ] Subject
- [X] Session
- [ ] Acquisition
- [ ] Analysis

### Inputs

- *image*
  - __Name__: image
  - __Type__: file
  - __Optional__: False
  - __Description__: A 2D or 3D image that is
[supported by SimpleITK](https://simpleitk.readthedocs.io/en/master/IO.html)

- *mask*
  - __Name__: mask
  - __Type__: file
  - __Optional__: True
  - __Description__: A mask to be used for extracting pyRadiomics features and is
supported by SimpleITK.  If no mask is passed, then a mask will be created that uses
the entire image except the top-left pixel that is located at (0,0).
PyRadiomics requires masks to have at least one pixel that has a different intensity
value than the rest.

- *params_file*
  - __Name__: params_file
  - __Type__: file
  - __Optional__: True
  - __Description__: A configuration file for configuring the pyRadiomics feature
extractor. If no file is passed, then the default configuration file will be used.

### Config

- *convert_numpy_to_single_channel*
  - __Name__: convert_numpy_to_single_channel
  - __Type__: boolean
  - __Description__: Converts 3D numpy arrays that have 3 images along the third
dimension--each representing a color channel--to single-channel arrays. Only the
first image will be used to calculate radiomic features.
  - __Default__: False
  - __Optional__: True

- *debug*
  - __Name__: debug
  - __Type__: boolean
  - __Description__: Log debug messages.
  - __Default__: False
  - __Optional__: True

- *voxel_based*
  - __Name__: voxel_based
  - __Type__: boolean
  - __Description__: Perform voxel-based calculations. The output is a SimpleITK image
of the parameter map instead of a float value for each feature. Voxel-based
calculations take a significant longer time; features have to be calculated for
each voxel.
  - __Default__: False
  - __Optional__: True

### Outputs

- __csvfile__: The output csv file with '_pyradiomics.csv' appended as the suffix.

## Contributing

For more information about how to get started contributing to that gear,
checkout [CONTRIBUTING.md](CONTRIBUTING.md).

[//]: # (## Table of Contents)

[//]: # ()
[//]: # (- [Usage Example]&#40;#usage-example&#41;)

[//]: # (- [Customizing Feature Extraction]&#40;#customizing-feature-extraction&#41;)

[//]: # (- [Documentation]&#40;#documentation&#41;)

[//]: # (- [Contributing]&#40;#contributing&#41;)

[//]: # (- [License]&#40;#license&#41;)

## Usage

## Usage examples

Please visit
[https://pyradiomics.readthedocs.io/en/latest/usage.html](https://pyradiomics.readthedocs.io/en/latest/usage.html)
for more information on how to use pyRadiomics.

### Prerequisites

Make sure you have a medical image (e.g., in NIfTI .nii or DICOM format)
and a corresponding segmentation mask.

### Custom Configuration File

PyRadiomics supports the use of a YAML configuration file
to customize feature extraction settings. Here’s an example configuration:

```yaml
setting:
# Settings to use, possible settings are listed in the documentation 
# (section "Customizing the extraction").
  resampledPixelSpacing: [1, 1, 1]
  interpolator: 'sitkBSpline'
  normalize: true
  normalizeScale: 100
  removeOutliers: 3
  binWidth: 25

# Image types to use: "Original" for unfiltered image,
# for possible filters, see documentation.
imageType:
  Original: {} # for dictionaries / mappings, None values are not allowed, 
  # '{}' is interpreted as an empty dictionary
featureClass:
  # redundant Compactness 1, Compactness 2 an Spherical Disproportion features
  # are disabled by default, they can be
  # enabled by specifying individual feature names (as is done for glcm)
  # and including them in the list.
  shape:
  firstorder: [] # specifying an empty list has the same effect as specifying nothing.
  glcm:  # Disable SumAverage by specifying all other GLCM features available
    - 'Autocorrelation'
    - 'JointAverage'
    - 'ClusterProminence'
    - 'ClusterShade'
    - 'ClusterTendency'
    - 'Contrast'
    - 'Correlation'
    - 'DifferenceAverage'
    - 'DifferenceEntropy'
    - 'DifferenceVariance'
    - 'JointEnergy'
    - 'JointEntropy'
    - 'Imc1'
    - 'Imc2'
    - 'Idm'
    - 'Idmn'
    - 'Id'
    - 'Idn'
    - 'InverseVariance'
    - 'MaximumProbability'
    - 'SumEntropy'
    - 'SumSquares'
  glrlm: # for lists none values are allowed, in this case, all features are enabled
  glszm:
  gldm:  # contains deprecated features, but as no individual features are specified,
    # the deprecated features are not enabled 
```

Save the configuration file as `params.yaml` and load it as shown below
in a project's session:
![params.yaml upload](docs/params_upload.png)

and select the file as the `params_file` input to run the pyradiomics gear
with the selected parameters:

![params.yaml upload](docs/params_selection.png)

## Acknowledgements

### Reference

Griethuysen, J. J. M., Fedorov, A., Parmar, C., Hosny, A., Aucoin, N., Narayan, V.,
Beets-Tan, R. G. H., Fillon-Robin, J. C., Pieper, S., Aerts, H. J. W. L. (2017).
Computational Radiomics System to Decode the Radiographic Phenotype. Cancer Research,
77(21), e104–e107.
`https://doi.org/10.1158/0008-5472.CAN-17-0339
<https://doi.org/10.1158/0008-5472.CAN-17-0339>`_

### Test data

Some image data for testing were generated by Rutherford, et al. under Creative Commons
Attribution 4.0 International License. Citation:

Rutherford, M., Mun, S.K., Levine, B. et al. A DICOM dataset for evaluation of
medical image de-identification. Sci Data 8, 183 (2021).
<https://doi.org/10.1038/s41597-021-00967-y>
