#!/usr/bin/env python
"""The run script."""
import logging
import sys

from flywheel_gear_toolkit import GearToolkitContext

# This design with a separate main and parser module
# allows the gear to be publishable and the main interfaces
# to it can then be imported in another project which enables
# chaining multiple gears together.
from fw_gear_pyradiomics.main import run
from fw_gear_pyradiomics.parser import parse_config
from fw_gear_pyradiomics.pyradwriters import PyradFlywheelWriter

# The run.py should be as minimal as possible.
# The gear is split up into 2 main components. The run.py file which is executed
# when the container runs. The run.py file then imports the rest of the gear as a
# module.


log = logging.getLogger(__name__)


def main(context: GearToolkitContext) -> None:  # pragma: no cover
    """Parses config, runs, and writes out output."""
    # Call the fw_gear_pyradiomics.parser.parse_config function
    # to extract the args, kwargs from the context (e.g. config.json).
    debug, pyrad_factory = parse_config(context)
    if debug:
        log.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)

    # # pprint(vars(pyrad_factory))
    # if validate_parameters_file(pyrad_factory.path_params):
    #     log.info("Parameters file is valid")
    #     e_code, features = run(pyrad_factory)
    # else:
    #     log.warning("Pyradiomics will run with default parameters.")
    #     pyrad_factory.path_params = {}
    e_code, features = run(pyrad_factory)

    # Get the writer based on the feature type and write the features
    pyrad_writer = PyradFlywheelWriter.factory(gear_context=context, features=features)
    # pyrad_writer.write_object_to_file_info(input_name="image")
    pyrad_writer.write_features_to_csv(input_name="image")

    # Exit the python script (and thus the container) with the exit
    # code returned by example_gear.main.run function.
    sys.exit(e_code)


# Only execute if file is run as main, not when imported by another module
if __name__ == "__main__":  # pragma: no cover
    # Get access to gear config, inputs, and sdk client if enabled.
    with GearToolkitContext() as gear_context:
        # Initialize logging, set logging level based on `debug` configuration
        # key in gear config.
        gear_context.init_logging()

        # Pass the gear context into main function defined above.
        main(gear_context)
